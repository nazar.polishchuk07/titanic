import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    df['Title'] = df['Name'].str.extract(r' ([A-Za-z]+\.)')

    
    titles_to_consider = ["Mr.", "Mrs.", "Miss."]


    results = []

    for title in titles_to_consider:
        
        title_data = df[df['Title'] == title]

        
        median_age = title_data['Age'].median()

        
        missing_count = title_data['Age'].isnull().sum()

    
        results.append((title, missing_count, round(median_age)))

    return results

